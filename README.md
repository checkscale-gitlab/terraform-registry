# TF Registry

This registry provides an implementation of the `provider.v1` Terraform Registry specification.

It supports uploading and retrieving Terraform providers from private sources.

This registry supports the following backends:
* S3 (AWS and non-AWS)
* Proxy (*without publish*)

## Generating PGP Keys

Terraform verifies provider packages using PGP keys.
In order to publish a provider, you will need to generate and sign the provider files.

**Generating the keys**

```shell script
$ gpg --gen-key --armor
```

**Exporting the keys**

```shell script
$ gpg --output public.pgp --armor --export <email used to generate>
```

## Publishing a provider

**Setup files**

```shell script
$ shasum -a 256 *.zip > terraform-provider-{NAME}_{VERSION}_SHA256SUMS
$ gpg --list-keys --keyid-format LONG
# expected output
pub   rsa3072/AA1B587BF225CBC2 2020-11-22 [SC] [expires: 2022-11-22] # the ID is the bit after rsa3072
      422209C5055229C4633AF075AA1B587BF225CBC2
uid                 [ultimate] TF signing key <test@example.org>
sub   rsa3072/EBA3CF1E1D2013A6 2020-11-22 [E] [expires: 2022-11-22]

# find the id of the key you created above
$ gpg --local-user {KEYID} --detach-sign terraform-provider-{NAME}_{VERSION}_SHA256SUMS
```

**Upload the files**

```shell script
$ curl \
    -X POST \
    -F "file=@./terraform-provider-{NAME}_{VERSION}_{OS}_{ARCH}.zip" \
    -F "checksum=@./terraform-provider-{NAME}_{VERSION}_SHA256SUMS" \
    -F "sig=@./terraform-provider-{NAME}_{VERSION}_SHA256SUMS.sig" \
    -F "file=@./public.pgp" \
    https://localhost:8080/v1/providers/{NAMESPACE}/{TYPE}/{VERSION}
```