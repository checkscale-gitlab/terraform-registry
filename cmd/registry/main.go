package main

import (
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/gorilla/mux"
	"github.com/namsral/flag"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/api"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/dao"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/registry"
	"net/http"
)

type registryOpts struct {
	proxyUrl    string
	s3bucket    string
	s3endpoint  string
	s3region    string
	s3pathStyle bool
}

func main() {
	port := flag.Int("port", 8080, "port to run on")
	debug := flag.Bool("debug", false, "enables debug logging")

	cert := flag.String("cert", "", "path to tls certificate")
	key := flag.String("key", "", "path to tls key")

	dbPath := flag.String("db-path", "./data.db", "path to the sqlite database")

	opts := &registryOpts{}

	registryType := flag.String("registry-type", "proxy", "which registry type to use (proxy, s3)")

	flag.StringVar(&opts.s3bucket, "s3-bucket", "", "s3 bucket to use")
	flag.StringVar(&opts.s3region, "s3-region", "us-west-1", "s3 region to use")
	flag.StringVar(&opts.s3endpoint, "s3-endpoint", "", "s3 endpoint to use")
	flag.BoolVar(&opts.s3pathStyle, "s3-path-style", false, "whether to use S3 DNS or path style buckets (e.g. mybucket.s3.amazonaws.com vs s3.amazonaws.com/mybucket)")

	flag.StringVar(&opts.proxyUrl, "proxy-registry", "https://registry.terraform.io", "registry to proxy requests to")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabled debug logging")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	// setup services
	db, err := dao.NewDatabaseManager(*dbPath)
	if err != nil {
		log.Fatal(err)
		return
	}

	rego := getProvider(db, *registryType, opts)

	disco := api.NewServiceDiscovery()
	provider := api.NewProvider(rego)

	// setup http routes
	router := mux.NewRouter()

	router.Handle("/.well-known/terraform.json", disco).Methods(http.MethodGet)
	router.HandleFunc("/v1/providers/{namespace}/{type}/versions", tracer.NewFunc(provider.ListAvailableVersions)).Methods(http.MethodGet)
	router.HandleFunc("/v1/providers/{namespace}/{type}/{version}/download/{os}/{arch}", tracer.NewFunc(provider.FindProviderPackage)).Methods(http.MethodGet)
	router.HandleFunc("/v1/providers/{namespace}/{type}/{version}", tracer.NewFunc(provider.PublishVersion)).Methods(http.MethodPost)
	router.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		log.Debugf("answering ping from %s", r.UserAgent())
		_, _ = w.Write([]byte("OK"))
	})

	// start the http server
	addr := fmt.Sprintf(":%d", *port)
	log.Infof("starting server on interface %s", addr)
	if *cert != "" && *key != "" {
		log.Info("starting server in secure mode")
		log.Fatal(http.ListenAndServeTLS(addr, *cert, *key, router))
	} else {
		log.Info("starting server in insecure mode")
		log.Fatal(http.ListenAndServe(addr, router))
	}
}

func getProvider(mgr *dao.DatabaseManager, rego string, opts *registryOpts) registry.ProviderRegistry {
	switch rego {
	default:
		fallthrough
	case "proxy":
		return registry.NewProxyRegistry(opts.proxyUrl)
	case "s3":
		return registry.NewS3Registry(&registry.S3Config{
			Bucket:    opts.s3bucket,
			Region:    opts.s3region,
			Endpoint:  opts.s3endpoint,
			PathStyle: opts.s3pathStyle,
		}, mgr)
	}
}
