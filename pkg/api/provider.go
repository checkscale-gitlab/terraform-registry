package api

import (
	"bytes"
	"context"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/registry"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/utils"
	"io"
	"mime/multipart"
	"net/http"
)

type Provider struct {
	impl registry.ProviderRegistry
}

func NewProvider(impl registry.ProviderRegistry) *Provider {
	p := new(Provider)
	p.impl = impl

	return p
}

func (p *Provider) PublishVersion(w http.ResponseWriter, r *http.Request) {
	requestId := tracer.GetRequestId(r)
	vars := mux.Vars(r)
	namespace := vars["namespace"]
	pType := vars["type"]
	version := vars["version"]
	if namespace == "" || pType == "" || version == "" {
		log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Debugf("rejecting request with empty namespace, type or version: {namespace: %s, type: %s, version: %s}", namespace, pType, version)
		http.Error(w, "namespace, type or version is empty", http.StatusBadRequest)
		return
	}
	log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Infof("publishing version %s of %s/%s", version, namespace, pType)
	// ensure we can parse the multipart upload
	if err := r.ParseMultipartForm(32 << 20); err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to parse file upload")
		http.Error(w, "failed to parse file upload", http.StatusBadRequest)
		return
	}
	file, header, err := p.getFormUpload(r.Context(), r, "file")
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to read uploaded file")
		http.Error(w, "failed to read uploaded file", http.StatusBadRequest)
		return
	}
	if _, err := utils.ParseProviderPackage(header.Filename); err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to parse name")
		http.Error(w, "failed to parse name", http.StatusBadRequest)
		return
	}

	checksum, _, err := p.getFormUpload(r.Context(), r, "checksum")
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to read uploaded checksum")
		http.Error(w, "failed to read uploaded checksum", http.StatusBadRequest)
		return
	}

	signature, _, err := p.getFormUpload(r.Context(), r, "sig")
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to read uploaded signature")
		http.Error(w, "failed to read uploaded signature", http.StatusBadRequest)
		return
	}

	pubKey, _, err := p.getFormUpload(r.Context(), r, "key")
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to read uploaded key")
		http.Error(w, "failed to read uploaded key", http.StatusBadRequest)
		return
	}

	// publish the package
	err = p.impl.PublishPackage(r.Context(), namespace, pType, version, &registry.PublishOpts{
		Filename: header.Filename,
		File:     file,
		Checksum: checksum,
		Sig:      signature,
		PubKey:   pubKey,
	})
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to publish provider")
		http.Error(w, "failed to publish file", http.StatusInternalServerError)
		return
	}
	log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Info("successfully published file")
}

func (p *Provider) getFormUpload(ctx context.Context, r *http.Request, name string) (*bytes.Buffer, *multipart.FileHeader, error) {
	requestId := tracer.GetContextId(ctx)
	file, header, err := r.FormFile(name)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to retrieve file")
		return nil, header, err
	}
	// read the signature
	var buf bytes.Buffer
	if bw, err := io.Copy(&buf, file); err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to read uploaded file")
		return nil, header, err
	} else {
		log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Infof("successfully read %d bytes from uploaded file", bw)
	}
	return &buf, header, nil
}

func (p *Provider) ListAvailableVersions(w http.ResponseWriter, r *http.Request) {
	requestId := tracer.GetRequestId(r)
	vars := mux.Vars(r)
	namespace := vars["namespace"]
	pType := vars["type"]
	if namespace == "" || pType == "" {
		log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Debugf("rejecting request with empty namespace or type: {namespace: %s, type: %s}", namespace, pType)
		http.Error(w, "namespace or type is empty", http.StatusBadRequest)
		return
	}
	log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Infof("listing versions of %s/%s", namespace, pType)

	versions, statusCode, err := p.impl.ListAvailableVersions(r.Context(), namespace, pType)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to list versions")
		http.Error(w, err.Error(), statusCode)
		return
	}
	httputils.ReturnJSON(w, http.StatusOK, &versions)
}

func (p *Provider) FindProviderPackage(w http.ResponseWriter, r *http.Request) {
	requestId := tracer.GetRequestId(r)
	vars := mux.Vars(r)
	namespace := vars["namespace"]
	pType := vars["type"]
	version := vars["version"]
	os := vars["os"]
	arch := vars["arch"]
	if namespace == "" || pType == "" || version == "" || os == "" || arch == "" {
		log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Debugf("rejecting request with empty parameter: {namespace: %s, type: %s, version: %s, os: %s, arch: %s}", namespace, pType, version, os, arch)
		http.Error(w, "missing parameter", http.StatusBadRequest)
		return
	}
	log.WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Infof("finding provider package %s/%s/%s/download/%s/%s", namespace, pType, version, os, arch)

	pkg, statusCode, err := p.impl.FindPackage(r.Context(), namespace, pType, version, os, arch)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"id": requestId, "path": r.URL.Path}).Error("failed to find provider package")
		http.Error(w, err.Error(), statusCode)
		return
	}
	httputils.ReturnJSON(w, http.StatusOK, pkg)
}
