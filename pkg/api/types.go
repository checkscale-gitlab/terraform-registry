package api

type DiscoveryDocument struct {
	ModulesV1   string `json:"modules.v1,omitempty"`
	ProvidersV1 string `json:"providers.v1,omitempty"`
	LoginV1     string `json:"login.v1,omitempty"`
}
