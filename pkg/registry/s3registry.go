package registry

import (
	"bytes"
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/hashicorp/terraform/registry/response"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/dao"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/utils"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/verify"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type S3Config struct {
	Bucket    string
	Region    string
	Endpoint  string
	PathStyle bool
}

type S3Registry struct {
	conf *S3Config
	mgr  *dao.DatabaseManager
}

func NewS3Registry(opts *S3Config, mgr *dao.DatabaseManager) *S3Registry {
	r := new(S3Registry)
	r.conf = opts
	r.mgr = mgr

	return r
}

func (r *S3Registry) ListAvailableVersions(ctx context.Context, namespace, pType string) (*response.TerraformProviderVersions, int, error) {
	requestId := tracer.GetContextId(ctx)

	records, err := r.mgr.ListVersions(namespace, pType)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to list versions")
		return nil, http.StatusInternalServerError, err
	}
	versionsToPlatform := map[string][]*response.TerraformProviderPlatform{}
	for _, o := range records {
		versionsToPlatform[o.Version] = append(versionsToPlatform[o.Version], &response.TerraformProviderPlatform{
			OS:   o.OS,
			Arch: o.Arch,
		})
	}
	var versions []*response.TerraformProviderVersion
	for k, v := range versionsToPlatform {
		versions = append(versions, &response.TerraformProviderVersion{
			Version:   k,
			Protocols: []string{"5.1"}, // assume everything is at least tf 0.13
			Platforms: v,
		})
	}
	log.Debugf("assembled version -> platform mappings: %v", versionsToPlatform)

	return &response.TerraformProviderVersions{
		ID:       fmt.Sprintf("%s/%s", namespace, pType),
		Versions: versions,
		Warnings: []string{},
	}, http.StatusOK, nil
}

func (r *S3Registry) FindPackage(ctx context.Context, namespace, pType, version, os, arch string) (*response.TerraformProviderPlatformLocation, int, error) {
	requestId := tracer.GetContextId(ctx)

	record, err := r.mgr.FindProvider(namespace, pType, version, os, arch)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to locate matching provider")
		return nil, http.StatusBadRequest, err // TODO we should check whether 404 is correct here
	}

	downloadURL, err := r.getPresigned(ctx, fmt.Sprintf("%s/%s", record.RegistryPath, record.Filename))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	shaPath := fmt.Sprintf("%s/%s_%s_SHA256SUMS", record.RegistryPath, record.Name, record.Version)
	shasumsURL, err := r.getPresigned(ctx, shaPath)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	signatureURL, err := r.getPresigned(ctx, fmt.Sprintf("%s.sig", shaPath))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	key, err := r.getFile(fmt.Sprintf("%s/%s_%s.asc", record.RegistryPath, record.Name, record.Version))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	pkg := &response.TerraformProviderPlatformLocation{
		Protocols:           []string{"5.1"},
		OS:                  os,
		Arch:                arch,
		Filename:            record.Filename,
		DownloadURL:         downloadURL,
		ShasumsURL:          shasumsURL,
		ShasumsSignatureURL: signatureURL,
		Shasum:              record.ShaSum,
		SigningKeys: response.SigningKeyList{
			GPGKeys: []*response.GPGKey{
				{
					ASCIIArmor: key.String(),
					Source:     "",
					SourceURL:  nil,
				},
			},
		},
	}
	return pkg, http.StatusOK, nil
}

func (r *S3Registry) PublishPackage(ctx context.Context, namespace, pType, version string, opts *PublishOpts) error {
	requestId := tracer.GetContextId(ctx)
	log.WithField("id", requestId).Infof("preparing to upload %s to %s/%s", opts.Filename, namespace, pType)
	fileKey := fmt.Sprintf("/%s/%s/%s/%s", namespace, pType, version, opts.Filename)
	name, err := utils.ParseProviderPackage(opts.Filename)
	// this will have been validate already, but double check
	if err != nil {
		return err
	}

	content := opts.File.Bytes()

	filesum, err := verify.GetSHA(content)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Errorf("failed to generate sha for file: %s", opts.Filename)
		return err
	}

	// create the database record
	_, err = r.mgr.AddProvider(&dao.ProviderRecord{
		Namespace:    namespace,
		Type:         pType,
		Version:      version,
		OS:           name.OS,
		Arch:         name.Arch,
		Name:         name.Name,
		Filename:     opts.Filename,
		ShaSum:       filesum,
		RegistryPath: fmt.Sprintf("/%s/%s/%s", namespace, pType, version),
	})
	if err != nil {
		log.WithError(err).WithField("id", requestId).Errorf("failed to persist database record")
		return err
	}

	s3client := s3.New(r.getSession())
	// upload the checksum list
	_, err = s3client.PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(r.conf.Bucket),
		Key:                aws.String(fmt.Sprintf("/%s/%s/%s/%s_%s_SHA256SUMS", namespace, pType, version, name.Name, name.Version)),
		Body:               bytes.NewReader(opts.Checksum.Bytes()),
		ContentDisposition: aws.String("attachment"),
	})
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to upload checksum list")
		return err
	}
	log.WithField("id", requestId).Info("successfully published checksum list")

	// upload the signed checksum list
	_, err = s3client.PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(r.conf.Bucket),
		Key:                aws.String(fmt.Sprintf("/%s/%s/%s/%s_%s_SHA256SUMS.sig", namespace, pType, version, name.Name, name.Version)),
		Body:               bytes.NewReader(opts.Sig.Bytes()),
		ContentDisposition: aws.String("attachment"),
	})
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to upload signed checksum list")
		return err
	}
	log.WithField("id", requestId).Info("successfully published checksum list signature")

	// upload the public key
	_, err = s3client.PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(r.conf.Bucket),
		Key:                aws.String(fmt.Sprintf("/%s/%s/%s/%s_%s.asc", namespace, pType, version, name.Name, name.Version)),
		Body:               bytes.NewReader(opts.PubKey.Bytes()),
		ContentDisposition: aws.String("attachment"),
	})
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to upload public key")
		return err
	}

	// upload the file
	_, err = s3client.PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(r.conf.Bucket),
		Key:                aws.String(fileKey),
		Body:               bytes.NewReader(content),
		ContentDisposition: aws.String("attachment"),
	})
	return err
}

func (r *S3Registry) getChecksumList(ctx context.Context, record *dao.ProviderRecord) (string, error) {
	requestId := tracer.GetContextId(ctx)

	key := fmt.Sprintf("/%s/%s/%s/%s_%s_SHA256SUMS", record.Namespace, record.Type, record.Version, record.Name, record.Version)
	log.WithField("id", requestId).Infof("attempting to retrieve checksums list: %s", key)

	s3client := s3.New(r.getSession())

	output, err := s3client.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(r.conf.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to retrieve checksums file from s3")
		return "", err
	}
	data, err := ioutil.ReadAll(output.Body)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Error("failed to read response body")
		return "", err
	}
	return string(data), nil
}

// getPresigned creates an S3 presigned download link
func (r *S3Registry) getPresigned(ctx context.Context, key string) (string, error) {
	requestId := tracer.GetContextId(ctx)
	log.WithField("id", requestId).Infof("generating pre-signed url for key: %s", key)
	s3client := s3.New(r.getSession())
	req, _ := s3client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(r.conf.Bucket),
		Key:    aws.String(key),
	})
	// sign for 1 minute, which should be plenty for Terraform to download
	signedURL, err := req.Presign(1 * time.Minute)
	if err != nil {
		log.WithError(err).WithField("id", requestId).Errorf("failed to pre-sign key: %s", key)
		return "", err
	}
	return signedURL, nil
}

func (r *S3Registry) getFile(key string) (*bytes.Buffer, error) {
	s3client := s3.New(r.getSession())
	output, err := s3client.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(r.conf.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		log.WithError(err).Error("failed to get object from s3")
		return nil, err
	}
	data, err := ioutil.ReadAll(output.Body)
	if err != nil {
		log.WithError(err).Error("failed to read response body")
		return nil, err
	}
	return bytes.NewBuffer(data), nil
}

// getSession creates a new S3 session
func (r *S3Registry) getSession() *session.Session {
	config := &aws.Config{
		S3ForcePathStyle:              aws.Bool(r.conf.PathStyle),
		Region:                        aws.String(r.conf.Region),
		CredentialsChainVerboseErrors: aws.Bool(true),
	}
	// special config if we're using a non-aws s3
	if r.conf.Endpoint != "" {
		if !strings.HasPrefix(r.conf.Endpoint, "https") {
			config.DisableSSL = aws.Bool(true)
		}
		config.Endpoint = aws.String(r.conf.Endpoint)
	}
	s, err := session.NewSession(config)
	if err != nil {
		log.WithError(err).Error("failed to create S3 session")
		panic(err) // we need to fail out hard on auth issues
	}
	return s
}
