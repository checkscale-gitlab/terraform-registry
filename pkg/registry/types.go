package registry

import "bytes"

type PublishOpts struct {
	Filename                    string
	File, Checksum, Sig, PubKey *bytes.Buffer
}
