/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package utils

import (
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/utils"
	"time"
)

// GormLogger is a custom logger for Gorm, making it us github.com/sirupsen/logrus
// https://github.com/onrik/gorm-logrus/blob/master/logger.go
type GormLogger struct {
	SlowThreshold         time.Duration
	SourceField           string
	SkipErrRecordNotFound bool
}

func NewGormLogger() *GormLogger {
	return &GormLogger{
		SkipErrRecordNotFound: true,
	}
}

func (l *GormLogger) LogMode(_ logger.LogLevel) logger.Interface {
	return l
}

func (l *GormLogger) Info(_ context.Context, s string, i ...interface{}) {
	log.Infof(s, i...)
}

func (l *GormLogger) Warn(_ context.Context, s string, i ...interface{}) {
	log.Warnf(s, i...)
}

func (l *GormLogger) Error(_ context.Context, s string, i ...interface{}) {
	log.Errorf(s, i...)
}

func (l *GormLogger) Trace(_ context.Context, begin time.Time, fc func() (string, int64), err error) {
	elapsed := time.Since(begin)
	sql, _ := fc()
	fields := log.Fields{}
	if l.SourceField != "" {
		fields[l.SourceField] = utils.FileWithLineNum()
	}
	if err != nil && !(errors.Is(err, gorm.ErrRecordNotFound) && l.SkipErrRecordNotFound) {
		fields[log.ErrorKey] = err
		log.WithFields(fields).Errorf("%s [%s]", sql, elapsed)
		return
	}

	if l.SlowThreshold != 0 && elapsed > l.SlowThreshold {
		log.WithFields(fields).Warnf("%s [%s]", sql, elapsed)
		return
	}

	log.WithFields(fields).Debugf("%s [%s]", sql, elapsed)
}

// Print handles log events from Gorm
func (*GormLogger) Print(v ...interface{}) {
	switch v[0] {
	case "sql":
		log.WithFields(log.Fields{
			"module":  "gorm",
			"type":    "sql",
			"rows":    v[5],
			"src_ref": v[1],
			"values":  v[4],
		}).Debug(v[3])
	case "log":
		log.WithFields(log.Fields{"module": "gorm", "type": "log"}).Print(v[2])
	}
}
