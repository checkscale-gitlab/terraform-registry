package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseProviderPackage(t *testing.T) {
	expected := &ProviderPackage{
		Name:    "terraform-provider-helm",
		Version: "1.3.2",
		OS:      "linux",
		Arch:    "amd64",
	}
	actual, err := ParseProviderPackage("terraform-provider-helm_1.3.2_linux_amd64.zip")
	assert.NoError(t, err)
	assert.EqualValues(t, expected, actual)
}

func TestParseProviderPackageWrongLength(t *testing.T) {
	actual, err := ParseProviderPackage("terraform-provider-helm.zip")
	assert.Error(t, err)
	assert.Nil(t, actual)
}

func TestProviderPackage_AsFilename(t *testing.T) {
	pkg := &ProviderPackage{
		Name:    "terraform-provider-helm",
		Version: "1.3.2",
		OS:      "linux",
		Arch:    "amd64",
	}
	expected := "terraform-provider-helm_1.3.2_linux_amd64.zip"
	assert.EqualValues(t, expected, pkg.AsFilename())
}
