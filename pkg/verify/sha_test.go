package verify

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUpdateChecksumList(t *testing.T) {
	list := `b46d607cedfefc94460bbff8a9d46e50f7dce364dc4050a2df81357159e07f81  terraform-provider-helm_1.3.2_linux_amd64.zip
`

	expected := `b46d607cedfefc94460bbff8a9d46e50f7dce364dc4050a2df81357159e07f81  terraform-provider-helm_1.3.2_linux_amd64.zip
totallyasha  test.zip`
	assert.EqualValues(t, expected, UpdateChecksumList(list, "test.zip", "totallyasha"))
}

func TestUpdateChecksumListBadEntriesAreRemoved(t *testing.T) {
	list := `b46d607cedfefc94460bbff8a9d46e50f7dce364dc4050a2df81357159e07f81  terraform-provider-helm_1.3.2_linux_amd64.zip
uhhhhhhhhhhhhhhhhhhhh
`

	expected := `b46d607cedfefc94460bbff8a9d46e50f7dce364dc4050a2df81357159e07f81  terraform-provider-helm_1.3.2_linux_amd64.zip
totallyasha  test.zip`
	assert.EqualValues(t, expected, UpdateChecksumList(list, "test.zip", "totallyasha"))
}
